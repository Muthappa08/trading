package com.example.lms.dto;

public class LeavesDto {

	private Long sickleaves;
	private Long flexiLeaves;
	public Long getSickleaves() {
		return sickleaves;
	}
	public void setSickleaves(Long sickleaves) {
		this.sickleaves = sickleaves;
	}
	public Long getFlexiLeaves() {
		return flexiLeaves;
	}
	public void setFlexiLeaves(Long flexiLeaves) {
		this.flexiLeaves = flexiLeaves;
	}
	@Override
	public String toString() {
		return "LeavesDto [sickleaves=" + sickleaves + ", flexiLeaves=" + flexiLeaves + "]";
	}
}
