package com.exhange.stocks;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.exchange.stocks.constants.ApplicationConstants;
import com.exchange.stocks.dto.BuyStockRequestDto;
import com.exchange.stocks.dto.RensponseDto;
import com.exchange.stocks.dto.StockDto;
import com.exchange.stocks.dto.StocksDto;
import com.exchange.stocks.entity.Stocks;
import com.exchange.stocks.entity.UserStocks;
import com.exchange.stocks.entity.Users;
import com.exchange.stocks.exception.NoStocksFoundException;
import com.exchange.stocks.exception.StockNotFoundException;
import com.exchange.stocks.exception.UserNotFoundException;
import com.exchange.stocks.repository.StockRepository;
import com.exchange.stocks.repository.UserRepository;
import com.exchange.stocks.repository.UserStocksRepository;
import com.exchange.stocks.servicesImpl.StocksServicesImpl;
import com.exchange.stocks.servicesImpl.UserServicesImpl;

@RunWith(MockitoJUnitRunner.Silent.class)
public class StocksTests {

	@InjectMocks
	UserServicesImpl userServicesImpl;
	@InjectMocks
	StocksServicesImpl stocksServicesImpl;
	@Mock
	UserRepository userRepository;
	@Mock
	UserStocksRepository userStocksRepository;
	@Mock
	StockRepository stockRepository;
	  
	@Test
	public void buyStock() {
		Users users=new Users();
		users.setUserId(3L);
		Mockito.when(userRepository.findById(3L)).thenReturn(Optional.of(users));
		Stocks stocks=new Stocks();
		LocalDate date = LocalDate.now();  
		stocks.setDate(date);
		stocks.setStockId(3L);
		stocks.setStockName("Titan");
		stocks.setStockPrice(800L);
		stocks.setStockQuantity(1200L);
		BuyStockRequestDto buyStockRequestDto=new BuyStockRequestDto();
		buyStockRequestDto.setStockId(3L);
		buyStockRequestDto.setStockQuantity(400L);
		Mockito.when(stockRepository.findById(buyStockRequestDto.getStockId())).thenReturn(Optional.of(stocks));
		RensponseDto rensponseDto2=new RensponseDto();
		rensponseDto2.setMessage(ApplicationConstants.STOCK_PURCHESED);
		rensponseDto2.setStatusCode(ApplicationConstants.STOCK_PURCHESED_CODE);
		RensponseDto rensponseDto1=userServicesImpl.buyStock(3L, buyStockRequestDto);
		assertEquals(rensponseDto1.getStatusCode(), rensponseDto2.getStatusCode());
	}
	@Test(expected=UserNotFoundException.class)
	public void buyStockNegative() {
		Mockito.when(userRepository.findById(3L)).thenReturn(Optional.ofNullable(null));
		BuyStockRequestDto buyStockRequestDto=new BuyStockRequestDto();
		buyStockRequestDto.setStockId(3L);
		buyStockRequestDto.setStockQuantity(400L);
		userServicesImpl.buyStock(3L, buyStockRequestDto);
	}
	
	@Test
	public void getPortfolio() {
		Users users=new Users();
		users.setUserId(3L);
		Mockito.when(userRepository.findById(3L)).thenReturn(Optional.of(users));
		UserStocks userStocks=new UserStocks();
		List<UserStocks> list=new ArrayList();
		LocalDate date = LocalDate.now();
		userStocks.setDate(date);
		userStocks.setStockId(3L);
		userStocks.setStockPrice(300L);
		userStocks.setStockQuantity(900L);
		userStocks.setUserStockId(1L);
		list.add(userStocks);
		Mockito.when(userStocksRepository.findByUserId(3L)).thenReturn(Optional.of(list));
		Stocks stocks=new Stocks();
		LocalDate date3 = LocalDate.now();  
		stocks.setDate(date3);
		stocks.setStockId(3L);
		stocks.setStockName("Titan");  
		stocks.setStockPrice(800L);
		stocks.setStockQuantity(1200L);
		Mockito.when(stockRepository.findById(3L)).thenReturn(Optional.of(stocks));
		List<StocksDto> stocksList = new ArrayList();
		StocksDto stockDto=new StocksDto();
		stocksList.add(stockDto);
		List<StocksDto> stocksList2	= userServicesImpl.getPortfolio(3L);  
		assertEquals(stocksList2.size(), stocksList.size());
	}
	@Test(expected=UserNotFoundException.class)
	public void getPortfolioNegative() {
		Mockito.when(userRepository.findById(3L)).thenReturn(Optional.ofNullable(null));
		userServicesImpl.getPortfolio(3L);
	}
	
	@Test
	public void searchByStockId() {
		Stocks stocks=new Stocks();
		LocalDate date3 = LocalDate.now();  
		stocks.setDate(date3);
		stocks.setStockId(3L);
		stocks.setStockName("Titan");  
		stocks.setStockPrice(800L);
		stocks.setStockQuantity(1200L);
		Mockito.when(stockRepository.findById(3L)).thenReturn(Optional.of(stocks));
		Optional<Stocks> stock2=stocksServicesImpl.searchByStockId(3L);
		assertEquals(stock2, Optional.of(stocks));
		
	}
	@Test(expected=StockNotFoundException.class)
	public void searchByStockIdNegative() {
		Mockito.when(stockRepository.findById(3L)).thenReturn(Optional.ofNullable(null));
		stocksServicesImpl.searchByStockId(3L);
		
	}
	@Test
	public void searchByStockName() {
		List<Stocks>list=new ArrayList();
		Stocks stocks=new Stocks();
		LocalDate date3 = LocalDate.now();  
		stocks.setDate(date3);
		stocks.setStockId(3L);
		stocks.setStockName("Titan");  
		stocks.setStockPrice(800L);
		stocks.setStockQuantity(1200L);
		list.add(stocks);
		List<StockDto> list1=new ArrayList();
		StockDto stockDto=new StockDto();
		list1.add(stockDto);
		Mockito.when(stockRepository.findByStockNameContaining("Tit")).thenReturn(Optional.of(list));
		Optional<List<StockDto>> list2=stocksServicesImpl.searchByStockName("Tit");
		assertEquals(list2.get().size(), Optional.of(list1).get().size());
		
	}
	@Test(expected=NoStocksFoundException.class)
	public void searchByStockNameNegative() {
		Mockito.when(stockRepository.findByStockNameContaining("Tit")).thenReturn(Optional.ofNullable(null));
		stocksServicesImpl.searchByStockName("Tit");
	}
}
