package com.exchange.stocks.exception;

public class StockNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public StockNotFoundException() {
		super();
	}

	public StockNotFoundException(final String message) {
		super(message);
	}
}
