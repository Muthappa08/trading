package com.exchange.stocks.exception;

public class LeavesAreLess extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public LeavesAreLess() {
		super();
	}

	public LeavesAreLess(final String message) {
		super(message);
	}
}
