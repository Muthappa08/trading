package com.exchange.stocks.exception;

public class StocksNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public StocksNotFoundException() {
		super();
	}

	public StocksNotFoundException(final String message) {
		super(message);
	}
}