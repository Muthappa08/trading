package com.exchange.stocks.exception;

public class NoStocksFoundException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public NoStocksFoundException() {
		super();
	}

	public NoStocksFoundException(final String message) {
		super(message);

	
	}}