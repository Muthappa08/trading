
package com.exchange.stocks.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.exchange.stocks.constants.ApplicationConstants;


@RestControllerAdvice
public class ExceptionHandlerControllerAdvice {

	@ExceptionHandler(Exception.class)
	public ExceptionResponse handleException(final Exception exception, final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(exception.getMessage());
		error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		return error; 
	}  
	@ExceptionHandler(StockNotFoundException.class)
	public ExceptionResponse handleInvalidStockId(final StockNotFoundException exception, final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(exception.getMessage());
	      error.setStatus(ApplicationConstants.STOCK_NOT_FOUND_CODE);
		return error; 
	}  
	@ExceptionHandler(UserNotFoundException.class)
	public ExceptionResponse handleInvalidUser(final UserNotFoundException exception, final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(exception.getMessage());
	      error.setStatus(ApplicationConstants.USER_NOT_FOUND_CODE);  
		return error; 
	}  
	@ExceptionHandler(StocksNotFoundException.class)
	public ExceptionResponse stocksNotFound(final StocksNotFoundException exception, final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(exception.getMessage());
	      error.setStatus(ApplicationConstants.STOCKS_NOT_FOUND_CODE);
		return error; 
	}  
	
	@ExceptionHandler(NoStocksFoundException.class)
	public ExceptionResponse stocksNotFoundSearch(final NoStocksFoundException exception, final HttpServletRequest request) {

		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(exception.getMessage());
	      error.setStatus(ApplicationConstants.STOCKS_NOT_FOUND_CODE);
		return error; 
	}  

}
