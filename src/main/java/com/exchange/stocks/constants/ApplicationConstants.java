package com.exchange.stocks.constants;

public class ApplicationConstants {

private ApplicationConstants() {
		
	}

	public static final Integer SUCCESS_CODE = 200;
	
			
	
	public static final Integer STOCK_NOT_FOUND_CODE = 600;
	public static final String STOCK_NOT_FOUND = "Stocks does not exist for this stockId";
	
	public static final Integer USER_NOT_FOUND_CODE = 601;  
	public static final String USER_NOT_FOUND = "User does not exist";
	
	public static final Integer STOCK_PURCHESED_CODE = 602;  
	public static final String STOCK_PURCHESED = "Stock purchesed successfully";
	public static final Integer STOCKS_NOT_FOUND_CODE = 606;
	public static final String STOCKS_NOT_FOUND = "Stocks does not exist for this user";
	
	public static final Integer NO_STOCK_FOUND_CODE = 608;
	public static final String NO_STOCK_FOUND = "Stocks does not exist for this name";
	
	
}
