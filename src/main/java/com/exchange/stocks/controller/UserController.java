package com.exchange.stocks.controller;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.exchange.stocks.dto.BuyStockRequestDto;
import com.exchange.stocks.dto.RensponseDto;
import com.exchange.stocks.dto.StockDto;
import com.exchange.stocks.dto.StocksDto;
import com.exchange.stocks.entity.Stocks;
import com.exchange.stocks.servicesImpl.UserServicesImpl;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	UserServicesImpl userServicesImpl; 

	/*
	 * @GetMapping("/{stockName}") public ResponseEntity<Optional<List<StockDto>>>
	 * search(@PathVariable("stockName")String stockName){ Optional<List<StockDto>>
	 * list=userServicesImpl.search(stockName); return new
	 * ResponseEntity<Optional<List<StockDto>>>(list, HttpStatus.OK); }
	 */
	
	
	   @PostMapping("/{userId}/buyStock") 
	   public ResponseEntity<RensponseDto> buystock(@PathVariable("userId") Long userId,@RequestBody BuyStockRequestDto buyStockRequestDto){
		   RensponseDto rensponseDto=  userServicesImpl.buyStock(userId,buyStockRequestDto);
		   return new ResponseEntity<RensponseDto>(rensponseDto, HttpStatus.OK);
	   }
	   @GetMapping("/{userId}/Stocks") 
	   public ResponseEntity<List<StocksDto>> portfolio(@PathVariable("userId") Long userId){
		   System.out.println("1");
		   List<StocksDto> stockList=  userServicesImpl.getPortfolio(userId);
		   return new ResponseEntity<List<StocksDto>>(stockList, HttpStatus.OK);
	   }
	 	 
}
