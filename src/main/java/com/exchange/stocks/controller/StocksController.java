package com.exchange.stocks.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.exchange.stocks.dto.StockDto;
import com.exchange.stocks.entity.Stocks;
import com.exchange.stocks.servicesImpl.StocksServicesImpl;

@RestController
@RequestMapping("/stocks")
public class StocksController {

	@Autowired
	StocksServicesImpl stocksServicesImpl; 
	@GetMapping("/{stockId}")
	public ResponseEntity<Optional<Stocks>> search(@PathVariable("stockId")Long stockId){
		Optional<Stocks> stockDto=stocksServicesImpl.searchByStockId(stockId);
		return new ResponseEntity<Optional<Stocks>>(stockDto, HttpStatus.OK);
	}
	@GetMapping  
	public ResponseEntity<Optional<List<StockDto>>> search2(@RequestParam("stockName")String stockName){
		Optional<List<StockDto>> stocklist=stocksServicesImpl.searchByStockName(stockName);
		return new ResponseEntity<Optional<List<StockDto>>>(stocklist, HttpStatus.OK);
	}
}
