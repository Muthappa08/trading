package com.exchange.stocks.entity;

import java.time.LocalDate;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="UserStocks")
@Entity
public class UserStocks {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long userStockId;
	private Long userId;
	private Long stockId;
	private Long stockQuantity;
	private Long stockPrice;
	private LocalDate date;
	public Long getUserStockId() {
		return userStockId;
	}
	public void setUserStockId(Long userStockId) {
		this.userStockId = userStockId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getStockId() {
		return stockId;
	}
	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}
	public Long getStockQuantity() {
		return stockQuantity;
	}
	public void setStockQuantity(Long stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	public Long getStockPrice() {
		return stockPrice;
	}
	public void setStockPrice(Long stockPrice) {
		this.stockPrice = stockPrice;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}

}
