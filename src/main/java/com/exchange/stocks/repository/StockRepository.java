package com.exchange.stocks.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exchange.stocks.entity.Stocks;

public interface StockRepository extends JpaRepository<Stocks, Long>{
	Optional<List<Stocks>> findByStockNameContaining(String place);
}
