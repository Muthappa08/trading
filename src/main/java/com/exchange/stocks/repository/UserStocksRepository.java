package com.exchange.stocks.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exchange.stocks.entity.Stocks;
import com.exchange.stocks.entity.UserStocks;

public interface UserStocksRepository extends JpaRepository<UserStocks, Long> {

	Optional<List<UserStocks>> findByUserId(Long userId);

}
