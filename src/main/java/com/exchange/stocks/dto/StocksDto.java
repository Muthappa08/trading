package com.exchange.stocks.dto;

import java.time.LocalDate;

public class StocksDto {

	private Long stockPrice;
	private String stockName;
	private Long stockQuantity;
	private LocalDate date;
	public Long getStockPrice() {
		return stockPrice;
	}
	public void setStockPrice(Long stockPrice) {
		this.stockPrice = stockPrice;
	}
	public String getStockName() {
		return stockName;
	}
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	public Long getStockQuantity() {
		return stockQuantity;
	}
	public void setStockQuantity(Long stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
}
