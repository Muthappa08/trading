package com.exchange.stocks.servicesImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exchange.stocks.constants.ApplicationConstants;
import com.exchange.stocks.dto.BuyStockRequestDto;
import com.exchange.stocks.dto.RensponseDto;
import com.exchange.stocks.dto.StockDto;
import com.exchange.stocks.dto.StocksDto;
import com.exchange.stocks.entity.Stocks;
import com.exchange.stocks.entity.UserStocks;
import com.exchange.stocks.entity.Users;
import com.exchange.stocks.exception.StocksNotFoundException;
import com.exchange.stocks.exception.UserNotFoundException;
import com.exchange.stocks.repository.StockRepository;
import com.exchange.stocks.repository.UserRepository;
import com.exchange.stocks.repository.UserStocksRepository;
import com.exchange.stocks.services.UserServices;

@Service
public class UserServicesImpl implements UserServices {

	@Autowired
	UserRepository userRepository;
	@Autowired
	UserStocksRepository userStocksRepository;
	@Autowired
	StockRepository stockRepository;
	ModelMapper mapper = new ModelMapper();

	public RensponseDto buyStock(Long userId, BuyStockRequestDto buyStockRequestDto) {
		Optional<Users> user = userRepository.findById(userId);
		Optional<Stocks> stocks = stockRepository.findById(buyStockRequestDto.getStockId());
		if (!user.isPresent()) {
			throw new UserNotFoundException(ApplicationConstants.USER_NOT_FOUND);
		}
		UserStocks userStocks = new UserStocks();
		userStocks.setDate(stocks.get().getDate());
		userStocks.setStockId(buyStockRequestDto.getStockId());
		userStocks.setStockPrice(stocks.get().getStockPrice());
		userStocks.setUserId(userId);
		userStocks.setStockQuantity(buyStockRequestDto.getStockQuantity());
		userStocksRepository.save(userStocks);
		RensponseDto rensponseDto = new RensponseDto();
		rensponseDto.setMessage(ApplicationConstants.STOCK_PURCHESED);
		rensponseDto.setStatusCode(ApplicationConstants.STOCK_PURCHESED_CODE);
		stocks.get().setStockQuantity(stocks.get().getStockQuantity() - buyStockRequestDto.getStockQuantity());
		stockRepository.save(stocks.get());
		return rensponseDto;
	}

	public List<StocksDto> getPortfolio(Long userId) {
		Optional<Users> user = userRepository.findById(userId);
		if (!user.isPresent()) {
			throw new UserNotFoundException(ApplicationConstants.USER_NOT_FOUND);
		}  

		Optional<List<UserStocks>> list = userStocksRepository.findByUserId(userId);	 

		List<StocksDto> stocksList = new ArrayList();
		for (UserStocks stocks : list.get()) {
			System.out.println(stocks);
			Optional<Stocks> stock = stockRepository.findById(stocks.getStockId());
			StocksDto stockDto = mapper.map(stocks, StocksDto.class);
			stockDto.setStockName(stock.get().getStockName());
			stocksList.add(stockDto);
		}
		return stocksList;
	}

}
