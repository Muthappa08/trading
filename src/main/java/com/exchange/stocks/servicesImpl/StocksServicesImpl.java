package com.exchange.stocks.servicesImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exchange.stocks.constants.ApplicationConstants;
import com.exchange.stocks.dto.StockDto;
import com.exchange.stocks.dto.StocksDto;
import com.exchange.stocks.entity.Stocks;
import com.exchange.stocks.entity.UserStocks;
import com.exchange.stocks.exception.NoStocksFoundException;
import com.exchange.stocks.exception.StockNotFoundException;
import com.exchange.stocks.repository.StockRepository;
import com.exchange.stocks.services.StocksServices;

@Service
public class StocksServicesImpl implements StocksServices{

	@Autowired
	StockRepository stockRepository;

	ModelMapper mapper=new ModelMapper();  
	public Optional<Stocks> searchByStockId(Long stockId) {
		Optional<Stocks> stocks=stockRepository.findById(stockId);
		if(!stocks.isPresent()) {
			throw new StockNotFoundException(ApplicationConstants.STOCK_NOT_FOUND);
		}
		return stocks;
	}

	public Optional<List<StockDto>> searchByStockName(String stockName) {
		Optional<List<Stocks>> stockslist=stockRepository.findByStockNameContaining(stockName);
		if(!stockslist.isPresent()) {
			throw new NoStocksFoundException(ApplicationConstants.NO_STOCK_FOUND);
		}
		List<StockDto> stockDtolist=new ArrayList();
		for (Stocks stocks : stockslist.get()) {  
			StockDto stockDto = mapper.map(stocks, StockDto.class);
			stockDtolist.add(stockDto);
		}
		return Optional.of(stockDtolist);
	}
	
}
